//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Valve.VR
{
    using System;
    using UnityEngine;
    
    
    public class SteamVR_Input_ActionSet_BTreeTraversal : Valve.VR.SteamVR_ActionSet
    {
        
        public virtual SteamVR_Action_Boolean IterateNext
        {
            get
            {
                return SteamVR_Actions.bTreeTraversal_IterateNext;
            }
        }
        
        public virtual SteamVR_Action_Boolean ReturnMenu
        {
            get
            {
                return SteamVR_Actions.bTreeTraversal_ReturnMenu;
            }
        }
    }
}
