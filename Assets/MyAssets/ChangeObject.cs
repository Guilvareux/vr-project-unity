﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeObject : MonoBehaviour
{
    private int currentMat;
    private Material lightcone;
    private Material defaultmat;
    // Start is called before the first frame update
    void Start()
    {
        //rend = GetComponent<Renderer>();
        defaultmat = GetComponent<Renderer>().material;

        //lightcone = (Material) Resources.Load("Assets/MyAssets/Stolen/LightCone_Blue.mat");
        //defaultmat = (Material) Resources.Load("Assets/MyAssets/Stolen/TorchHead.mat");
        currentMat = 0;        
    }

    // Update is called once per frame
    void Update()
    {
        SwitchMaterial();        
    }

    void OnCollisionEnter(Collision collision)
    {
        SwitchMaterial();
    }

    void SwitchMaterial()
    {
        if(currentMat == 0)
        {
            defaultmat.color = Color.red;
            currentMat = 1;
        }
        else
        {
            defaultmat.color = Color.white;
            currentMat = 0;         
        }
    }
}
