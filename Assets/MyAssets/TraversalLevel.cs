﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Valve.VR;

public class TraversalLevel : MonoBehaviour
{
    public static TraversalLevel Instance {get; private set;}
    
    //Delegates and Events
    public delegate void nextText();
    public static event nextText iterate;

    public delegate void hitNode(int value, GameObject node);
    public static event hitNode registerHit;

    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean next = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Next");
    
    //3D Nodes
    public GameObject prefab;
    public GameObject connectionPrefab;
    public List<GameObject> nodeList;
    private static List<Vector3> loc = new List<Vector3>();

    private List<int> hitOrder = new List<int>();
    private List<int> traversalOrder;
    //private int[] abusedNodes;
    int tutorialStage = 0;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void NextIteration(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        iterate();
    }


    // Start is called before the first frame update
    void Start()
    {
        next.AddOnStateDownListener(NextIteration, handType);
        BTree tree = new BTree();
        int[] tutorialTree = {10, 30, 90, 39, 59, 23};
        tree.InsertArray(tutorialTree);
        Scene scene = SceneManager.GetActiveScene();
        if(scene.name == "InOrderTraversal")
        {
            traversalOrder = tree.getInOrderTraversal(tree.root);
            iterate += inordertutorial;
        }
        else if(scene.name == "PreOrderTraversal")
        {
            traversalOrder = tree.getPreOrderTraversal(tree.root);
            iterate += preordertutorial;
        }
        else if(scene.name == "PostOrderTraversal")
        {
            traversalOrder = tree.getPostOrderTraversal(tree.root);
            iterate += postordertutorial;
        }
        drawTree(tree);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void drawTree(BTree tree)
    {
        loc = tree.orderTree(tree.root, new Vector3(0, 2, 2.0f), 1.0f);
        drawNode(tree.root, 0);
    }

    public void drawNode(BNode node, int height)
    {
        height++;
        Debug.Log(loc.Count);
        Vector3 location = loc[0];
        GameObject go = (GameObject) Instantiate(prefab, loc[0], Quaternion.identity);
        go.GetComponent<CloneNode>().value = node.nodeValue;
        go.SetActive(false);
        nodeList.Add(go);
        loc.RemoveAt(0);
        float diff = 0.5f;
        for(int i=0; i<height; i++)
        {
            diff = diff / 2;
        }
        int anglediff = 15 * height;
        if(node.Left != null)
        {
            int angle = 75 - anglediff;
            Quaternion ihatequaternions = Quaternion.AngleAxis(angle * -1, Vector3.forward);
            Instantiate(connectionPrefab, location + new Vector3(-diff, -0.125f ,0), ihatequaternions);
            drawNode(node.Left, height);
        }
        if(node.Right != null)
        {
            Quaternion ihatequaternions = Quaternion.AngleAxis(75 - anglediff, Vector3.forward);
            Instantiate(connectionPrefab, location + new Vector3(diff, -0.125f ,0), ihatequaternions);            
            drawNode(node.Right, height);
        }                                        
    }

    public void firstTraversal(int value, GameObject node)
    {
        Debug.Log("NODE HIT " + value);
        int counter = hitOrder.Count;
        if(traversalOrder.Count > hitOrder.Count)
        {
            if(traversalOrder[counter] == value)
            {
                hitOrder.Add(value);
                node.GetComponent<Renderer>().material.color = Color.green;
                node.GetComponent<CloneNode>().playPositive();
                iterate();                
            }
            else
            {
                Debug.Log("TRY AGAIN");
                node.GetComponent<CloneNode>().playNegative();
            }
            if(traversalOrder.Count == hitOrder.Count)
            {
                int[] traversal = new int[traversalOrder.Count];
                int[] order = new int[hitOrder.Count];
                for(int i=0; i<traversalOrder.Count; i++)
                {
                    traversal[i] = traversalOrder[i];
                    order[i] = hitOrder[i];
                }
                if(traversalOrder.SequenceEqual(hitOrder))
                {
                    Debug.Log("Correct Order!");
                    registerHit -= firstTraversal;
                    SceneManager.LoadScene("Menu");
                }
                else
                {
                    Debug.Log("This shouldn't be possible");
                }
            }
        }
        else
        {
            Debug.Log("Traversal Complete");
                    
        }      
    }

    public IEnumerator flashWin()
    {
        int counter = 0;
        while(counter < 5)
        {
            for(int i=0; i<nodeList.Count; i++)
            {
                nodeList[i].GetComponent<Renderer>().material.color = Color.white;          
            }
            yield return new WaitForSeconds(1);
            for(int i=0; i<nodeList.Count; i++)
            {
                nodeList[i].GetComponent<Renderer>().material.color = Color.green;          
            }
        }
    }

    public void inordertutorial()
    {
        tutorialStage++;
        switch(tutorialStage)
        {
            case 1:
                nodeList[0].SetActive(true);
                nodeList[1].SetActive(true);
                nodeList[5].SetActive(true);
                break;

            case 2:
                nodeList[0].GetComponent<Renderer>().material.color = Color.yellow;
                break;

            case 3:
                nodeList[0].GetComponent<Renderer>().material.color = Color.white;
                nodeList[1].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[2].SetActive(true);
                nodeList[3].SetActive(true);
                break;

            case 4:
                nodeList[1].GetComponent<Renderer>().material.color = Color.white;
                nodeList[2].GetComponent<Renderer>().material.color = Color.yellow;
                
                break;

            case 6:
                nodeList[2].GetComponent<CloneNode>().showValue(true);
                nodeList[2].GetComponent<Renderer>().material.color = Color.green;
                break;

            case 8:
                nodeList[1].GetComponent<CloneNode>().showValue(true);
                nodeList[1].GetComponent<Renderer>().material.color = Color.green;
                nodeList[3].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[4].SetActive(true);
                break;

            case 9:            
                nodeList[3].GetComponent<Renderer>().material.color = Color.white;
                nodeList[4].GetComponent<Renderer>().material.color = Color.yellow;
                break;

            case 10:
                nodeList[4].GetComponent<Renderer>().material.color = Color.green;
                nodeList[4].GetComponent<CloneNode>().showValue(true);
                break;

            case 11:
                nodeList[3].GetComponent<Renderer>().material.color = Color.green;
                nodeList[3].GetComponent<CloneNode>().showValue(true);
                break;


            case 13:
                nodeList[0].GetComponent<Renderer>().material.color = Color.green;
                nodeList[0].GetComponent<CloneNode>().showValue(true);
                break;

            case 14:
                nodeList[5].GetComponent<Renderer>().material.color = Color.green;
                nodeList[5].GetComponent<CloneNode>().showValue(true);
                break;

            case 17:
                for(int i=0; i<nodeList.Count; i++)
                {
                    nodeList[i].GetComponent<CloneNode>().showValue(false);
                    nodeList[i].GetComponent<CloneNode>().setHittable(true);
                    nodeList[i].GetComponent<Renderer>().material.color = Color.white;
                }
                StartCoroutine(inorderanimate());
                iterate -= inordertutorial;
                
                break;
        }
    }

    IEnumerator inorderanimate()
    {
        while(true)
        {
            nodeList[5].GetComponent<Renderer>().material.color = Color.white;
            nodeList[0].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[0].GetComponent<Renderer>().material.color = Color.white;
            nodeList[1].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[1].GetComponent<Renderer>().material.color = Color.white;
            nodeList[2].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[2].GetComponent<Renderer>().material.color = Color.white;
            nodeList[4].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[4].GetComponent<Renderer>().material.color = Color.white;
            nodeList[3].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[3].GetComponent<Renderer>().material.color = Color.white;
            nodeList[5].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
        }
    }

    public void postordertutorial()
    {
        tutorialStage++;
        switch(tutorialStage)
        {
            case 1:
                nodeList[0].SetActive(true);
                nodeList[1].SetActive(true);
                nodeList[5].SetActive(true);
                nodeList[0].GetComponent<Renderer>().material.color = Color.yellow;
                break;

            case 2:
                nodeList[0].GetComponent<Renderer>().material.color = Color.white;
                nodeList[1].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[2].SetActive(true);
                nodeList[3].SetActive(true);
                break;

            case 3:
                nodeList[1].GetComponent<Renderer>().material.color = Color.white;
                nodeList[2].GetComponent<Renderer>().material.color = Color.yellow;
                break;

            case 7:
                nodeList[2].GetComponent<Renderer>().material.color = Color.green;
                nodeList[2].GetComponent<CloneNode>().showValue(true);
                nodeList[1].GetComponent<Renderer>().material.color = Color.yellow;           
                break;

            case 8:
                nodeList[3].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[1].GetComponent<Renderer>().material.color = Color.white;
                nodeList[4].SetActive(true);
                break;

            case 9:
                nodeList[3].GetComponent<Renderer>().material.color = Color.white;
                nodeList[4].GetComponent<Renderer>().material.color = Color.yellow;
                break;

            case 10:
                nodeList[4].GetComponent<Renderer>().material.color = Color.green;
                nodeList[4].GetComponent<CloneNode>().showValue(true);
                break;
            case 11:
                nodeList[3].GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case 12:
                nodeList[3].GetComponent<CloneNode>().showValue(true);
                nodeList[3].GetComponent<Renderer>().material.color = Color.green;
                break;
            case 13:
                nodeList[1].GetComponent<CloneNode>().showValue(true);
                nodeList[1].GetComponent<Renderer>().material.color = Color.green;
                break;
            case 14:
                nodeList[0].GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case 15:
                nodeList[5].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[0].GetComponent<Renderer>().material.color = Color.white;
                break;
            case 16:
                nodeList[5].GetComponent<CloneNode>().showValue(true);
                nodeList[5].GetComponent<Renderer>().material.color = Color.green;
                break;
            case 17:
                nodeList[0].GetComponent<Renderer>().material.color = Color.green;
                nodeList[0].GetComponent<CloneNode>().showValue(true);
                break;

            case 19:
                for(int i=0; i<nodeList.Count; i++)
                {
                    nodeList[i].GetComponent<CloneNode>().showValue(false);
                    nodeList[i].GetComponent<CloneNode>().setHittable(true);
                    nodeList[i].GetComponent<Renderer>().material.color = Color.white;
                }
                StartCoroutine(postorderanimate());
                iterate -= postordertutorial;
                break;
        }
    }

    IEnumerator postorderanimate()
    {
        while(true)
        {
            nodeList[0].GetComponent<Renderer>().material.color = Color.white;
            nodeList[2].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[2].GetComponent<Renderer>().material.color = Color.white;
            nodeList[4].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[4].GetComponent<Renderer>().material.color = Color.white;
            nodeList[3].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[3].GetComponent<Renderer>().material.color = Color.white;
            nodeList[1].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[1].GetComponent<Renderer>().material.color = Color.white;
            nodeList[5].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[5].GetComponent<Renderer>().material.color = Color.white;
            nodeList[0].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
        }
    }

    public void preordertutorial()
    {
        tutorialStage++;
        switch(tutorialStage)
        {
            case 1:
                nodeList[0].SetActive(true);
                nodeList[1].SetActive(true);
                nodeList[5].SetActive(true);
                nodeList[0].GetComponent<Renderer>().material.color = Color.yellow;
                break;

            case 2:
                nodeList[0].GetComponent<Renderer>().material.color = Color.green;
                nodeList[0].GetComponent<CloneNode>().showValue(true);
                break;

            case 3:
                nodeList[1].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[2].SetActive(true);
                nodeList[3].SetActive(true);
                break;
            case 4:
                nodeList[1].GetComponent<Renderer>().material.color = Color.green;
                nodeList[1].GetComponent<CloneNode>().showValue(true);
                break;
            case 5:
                nodeList[2].GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case 6:
                nodeList[2].GetComponent<Renderer>().material.color = Color.green;
                nodeList[2].GetComponent<CloneNode>().showValue(true);
                break;
            case 7:
                nodeList[1].GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case 8:
                nodeList[1].GetComponent<Renderer>().material.color = Color.green;
                nodeList[3].GetComponent<Renderer>().material.color = Color.yellow;
                nodeList[4].SetActive(true);
                break;
            case 9:
                nodeList[3].GetComponent<Renderer>().material.color = Color.green;
                nodeList[3].GetComponent<CloneNode>().showValue(true);
                break;
            case 10:
                nodeList[4].GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case 11:
                nodeList[4].GetComponent<Renderer>().material.color = Color.green;
                nodeList[4].GetComponent<CloneNode>().showValue(true);
                break;
            case 12:
                nodeList[5].GetComponent<Renderer>().material.color = Color.green;
                nodeList[5].GetComponent<CloneNode>().showValue(true);
                break;
            case 15:
                for(int i=0; i<nodeList.Count; i++)
                {
                    nodeList[i].GetComponent<CloneNode>().showValue(false);
                    nodeList[i].GetComponent<CloneNode>().setHittable(true);
                    nodeList[i].GetComponent<Renderer>().material.color = Color.white;
                }
                StartCoroutine(preorderanimate());
                iterate -= preordertutorial;
                break;
        }
    }
    IEnumerator preorderanimate()
    {
        while(true)
        {
            nodeList[5].GetComponent<Renderer>().material.color = Color.white;
            nodeList[0].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[0].GetComponent<Renderer>().material.color = Color.white;
            nodeList[1].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[1].GetComponent<Renderer>().material.color = Color.white;
            nodeList[2].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[2].GetComponent<Renderer>().material.color = Color.white;
            nodeList[3].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[3].GetComponent<Renderer>().material.color = Color.white;
            nodeList[4].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
            nodeList[4].GetComponent<Renderer>().material.color = Color.white;
            nodeList[5].GetComponent<Renderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.7f);
        }
    }

    void OnDestroy()
    {
        next.RemoveOnStateDownListener(NextIteration, handType);
    }
}
