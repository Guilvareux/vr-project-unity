﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeTrial : MonoBehaviour
{
    public static TimeTrial Instance {get; private set;}

    public GameObject connectionPrefab;
    public GameObject startPrefab;
    public GameObject prefab;

    //Game Loop
    List<int> traversalOrder;
    List<int> hitOrder;
    BTree currentTree;


    private int correctTraversals = 0;

    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //startGame();
        traversalOrder = new List<int>();
        hitOrder = new List<int>();    
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
        
    }
    void nextTree()
    {
        if(traversalOrder.Count > 1){traversalOrder.Clear();}
        if(hitOrder.Count > 1){hitOrder.Clear();}

        currentTree = new BTree();
        currentTree.createRandomTree();
        int rand = Random.Range(0,9); 
        if(rand <= 3)
        {
            traversalOrder = currentTree.getPreOrderTraversal(currentTree.root);
            GameObject.Find("TraversalInfo").GetComponent<TextMeshPro>().SetText("Pre-Order:\nRead, Left, Right"); 
        }
        else if(rand <= 6)
        {
            traversalOrder = currentTree.getPostOrderTraversal(currentTree.root);
            GameObject.Find("TraversalInfo").GetComponent<TextMeshPro>().SetText("Post-Order:\nLeft, Right, Read");
        }
        else
        {
            traversalOrder = currentTree.getInOrderTraversal(currentTree.root);
            GameObject.Find("TraversalInfo").GetComponent<TextMeshPro>().SetText("In-Order:\nLeft, Read, Right");
        }
        drawTree(currentTree);     
    }

    public void drawTree(BTree tree)
    {
        List<Vector3> loc = tree.orderTree(tree.root, new Vector3(0, 2, 0.5f), 1.0f);
        drawNode(tree.root, loc, 0);
    }

    public void drawNode(BNode node, List<Vector3> loc, int height)
    {
        height++;
        Vector3 location = loc[0];
        GameObject go = (GameObject) Instantiate(prefab, loc[0], Quaternion.identity);
        CloneNode cl = go.GetComponent<CloneNode>();
        cl.value = node.nodeValue;
        cl.setHittable(true);
        //go.SetActive(false);
        loc.RemoveAt(0);
        float diff = 0.5f;
        for(int i=0; i<height; i++)
        {
            diff = diff / 2;
        }
        int anglediff = 15 * height;
        if(node.Left != null)
        {
            int angle = 75 - anglediff;
            Quaternion ihatequaternions = Quaternion.AngleAxis(angle * -1, Vector3.forward);
            Instantiate(connectionPrefab, location + new Vector3(-diff, -0.125f ,0), ihatequaternions);
            drawNode(node.Left, loc, height);
        }
        if(node.Right != null)
        {
            Quaternion ihatequaternions = Quaternion.AngleAxis(75 - anglediff, Vector3.forward);
            Instantiate(connectionPrefab, location + new Vector3(diff, -0.125f ,0), ihatequaternions);            
            drawNode(node.Right, loc, height);
        }                                        
    }

    public void startGame()
    {
        Destroy(GameObject.Find("StartCube"));
        StartCoroutine(countIn());
    }

    IEnumerator countIn()
    {
        GameObject.Find("TimeTrialText").GetComponent<TextMeshPro>().SetText("Good Luck!");
        yield return new WaitForSeconds(3);
        for(int i=5; i>0; i--)
        {
            GameObject.Find("TimeTrialText").GetComponent<TextMeshPro>().SetText(i.ToString());
            yield return new WaitForSeconds(1);
        }
        StartCoroutine(Counter());
        nextTree();
    }

    IEnumerator Counter()
    {
        int timeLeft = 60;
        while(timeLeft > 0)
        {
            timeLeft--;
            GameObject.Find("TimeTrialText").GetComponent<TextMeshPro>().SetText("Time Remaining: " + timeLeft);
            yield return new WaitForSeconds(1);
        }
        finishGame();
    }

    IEnumerator MyCoroutine()
    {
        Debug.Log("I've got a song that'll get on your nerves");
        for(int i=0; i<5; i++)
        {
            Debug.Log("get on your nerves");

            yield return new WaitForSeconds(5);
        }
    }

    public void finishGame()
    {
        GameObject[] nodes = GameObject.FindGameObjectsWithTag("Node");
        for(int i=0; i<nodes.Length; i++)
        {
            Destroy(nodes[i]);
            //nodes[i].SetActive = false;
        }
        GameObject[] cons = GameObject.FindGameObjectsWithTag("Connector");
        for(int i=0; i<cons.Length; i++)
        {
            Destroy(cons[i]);
        }
        Destroy(GameObject.Find("TraversalInfo"));
        Destroy(GameObject.Find("TimeTrialText"));

        GameObject.Find("TextResult").GetComponent<TextMeshPro>().SetText("Your score was: \n" + correctTraversals);
        GameObject.Find("TextResult").GetComponent<MeshRenderer>().enabled = true;

    }

    IEnumerator returnToMenu()
    {
        //Animator animator;
        //animator.SetTrigger("FadeOut");
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene("Menu");
    }

    public void traversal(int value, GameObject node)
    {
        Debug.Log("NODE HIT " + value);
        int counter = hitOrder.Count;
        if(traversalOrder.Count > hitOrder.Count)
        {
            if(traversalOrder[counter] == value)
            {
                hitOrder.Add(value);
                node.GetComponent<Renderer>().material.color = Color.green;
                node.GetComponent<CloneNode>().playPositive();
                //iterate();
            }
            else
            {
                Debug.Log("TRY AGAIN");
                node.GetComponent<CloneNode>().playNegative();
            }
            if(traversalOrder.Count == hitOrder.Count)
            {
                int[] traversal = new int[traversalOrder.Count];
                int[] order = new int[hitOrder.Count];
                for(int i=0; i<traversalOrder.Count; i++)
                {
                    traversal[i] = traversalOrder[i];
                    order[i] = hitOrder[i];
                }
                if(traversalOrder.SequenceEqual(hitOrder))
                {
                    Debug.Log("Correct Order!");
                    correctTraversals++;
                    GameObject[] oldnodes = GameObject.FindGameObjectsWithTag("Node");
                    for(int i=0; i<oldnodes.Length; i++)
                    {
                        Destroy(oldnodes[i]);
                    }
                    GameObject[] cons = GameObject.FindGameObjectsWithTag("Connector");
                    for(int i=0; i<cons.Length; i++)
                    {
                        Destroy(cons[i]);
                    }
                    GameObject.Find("TreeCompleteSound").GetComponent<AudioSource>().Play();
                    StartCoroutine(waitForNextTree());
                }
                else
                {
                    Debug.Log("This shouldn't be possible");
                }
            }
        }
        else
        {
            Debug.Log("Traversal Complete");
                    
        }      
    }

    IEnumerator waitForNextTree()
    {
        yield return new WaitForSeconds(1);
        nextTree();
    }

}
