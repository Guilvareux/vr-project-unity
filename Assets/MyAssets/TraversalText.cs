﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class TraversalText : MonoBehaviour
{
    int currentText = 0;
    public String[] messages = new String[50];
    // Start is called before the first frame update
    void Start()
    {
        Scene scene = SceneManager.GetActiveScene();
        if(scene.name == "PreOrderTraversal"){setpreorder();}
        if(scene.name == "PostOrderTraversal"){setpostorder();}
        if(scene.name =="InOrderTraversal"){setinorder();}
        GetComponent<TextMeshPro>().SetText(messages[0]);
        TraversalLevel.iterate += nextText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void nextText()
    {
            Debug.Log(messages[currentText]);
            currentText++;
            GetComponent<TextMeshPro>().SetText(messages[currentText]);
    }

    void setinorder()
    {
        messages[0] = "In an InOrder traversal, the order of execution is Left, Read, Right";
        messages[1] = "Let's walkthrough it";
        messages[2] = "Starting from the root node our first step is to move left";
        messages[3] = "From here, if there is a node, move to the left.";
        messages[4] = "Again, if there is a node move to the left.";
        messages[5] = "If there is no node to the left, we must finish the execution on the current node.";
        messages[6] = "So we read the value of the node we're at";
        messages[7] = "and move back up the tree";
        messages[8] = "We now read the parent and move right.";
        messages[9] = "Now remember Left, Read Right. Our first move is left.";
        messages[10] = "We can't move left again so we read this node.";
        messages[11] = "We move back up the tree and read the parent.";
        messages[12] = "Now we move back up to the node which hasn't been completed yet, the root.";
        messages[13] = "we read the root and move to the right";
        messages[14] = "We read the last node.";
        messages[15] = "Fantastic. That's an in-order traversal";
        messages[16] = "Remember. An InOrder Traversal is Left, Root, Right.";
        messages[17] = "Here's the order again, hit menu when you are ready to return to the menu";
    }

    void setpreorder()
    {
        messages[0] = "In an PreOrder traversal, the order of execution is Read, Left, Right";
        messages[1] = "Let's walkthrough it";
        messages[2] = "Starting from the root node our first step is to read";
        messages[3] = "From here, if there is a node, move to the left.";
        messages[4] = "Again, we read this node.";
        messages[5] = "If there is a node to the left, we move left.";
        messages[6] = "Read the value of the node.";
        messages[7] = "and move back up the tree";
        messages[8] = "Next we move right.";
        messages[9] = "we read the node we've traveled to.";
        messages[10] = "We move left";
        messages[11] = "read.";
        messages[12] = "Now we read the final node.";
        messages[13] = "Fantastic. That's a pre-order traversal";
        messages[14] = "Remember. An pre-Order Traversal is Read, Left, Right";
        messages[15] = "Here's the order again, hit menu when you are ready to return to the menu";
    }

    void setpostorder()
    {
        messages[0] = "In an PostOrder traversal, the order of execution is Left, Right, Read";
        messages[1] = "Let's walkthrough it";
        messages[2] = "Starting from the root node our first step is to move left.";
        messages[3] = "From here, if there is a node, move to the left.";
        messages[4] = "Again, we move left if there's one free.";
        messages[5] = "IF there isn't, we move right.";
        messages[6] = "If we can't move right, we read the node.";
        messages[7] = "and move back up the tree to the parent node.";
        messages[8] = "Next we move right.";
        messages[9] = "can we move left? yes, so move left";
        messages[10] = "can we move left or right frome here? No, so read.";
        messages[11] = "move up the tree";
        messages[12] = "read the parent if we can't move left or right.";
        messages[13] = "once again, if we can't move left or right, read.";
        messages[14] = "Now we're back at the root, we have to complete the operation and move right";
        messages[15] = "Read this node";
        messages[16] = "and finally read the root node last.";
        messages[17] = "";
        messages[18] = "Here's the traversal again, but quicker. Hit the menu node when you're ready to return to the menu.";
    }

    void OnDestroy()
    {
        TraversalLevel.iterate -= nextText;
    }
}
