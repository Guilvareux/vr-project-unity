﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraversalSphereScript : MonoBehaviour
{
    GameObject menuprefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollision()
    {
        GameObject[] spheres = GameObject.FindGameObjectsWithTag("MenuSphere");
        for(int i=0; i<spheres.Length; i++)
        {
            Destroy(spheres[i]);
        }
    }
}
