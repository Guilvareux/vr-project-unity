﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializePlayer : MonoBehaviour
{
    public GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        if(GameObject.FindWithTag("Player") == null)
        {
            Instantiate(playerPrefab);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}