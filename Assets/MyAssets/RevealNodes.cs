﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RevealNodes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(revealNodes());        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
        
    }

    IEnumerator revealNodes()
    {
        GameObject[] nodes = GameObject.FindGameObjectsWithTag("MenuSphere");
        yield return new WaitForSeconds(1);
        Debug.Log(nodes.Length);
        for(int i=0; i<nodes.Length; i++)
        {
            nodes[i].GetComponent<MeshRenderer>().enabled = true;
            nodes[i].GetComponent<SphereCollider>().enabled = true;
        }        
    }
}
