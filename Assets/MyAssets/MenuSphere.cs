﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSphere : MonoBehaviour
{
    public string scenename;
    public string textname;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponentInChildren<TextMeshPro>().SetText(textname);        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        SceneManager.LoadScene(scenename);
    }
}
