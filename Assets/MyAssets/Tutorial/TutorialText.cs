﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class TutorialText : MonoBehaviour
{
    int currentText = 0;
    public String[] messages = new String[100];
    // Start is called before the first frame update
    void Start()
    {
        setMessages();
        TutorialLevel.iterate += nextText;               
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void nextText()
    {
        Debug.Log(messages[currentText]);
        currentText++;
        GetComponent<TextMeshPro>().SetText(messages[currentText]);
    }

    void setMessages()
    {
        messages[0] = "Let’s start with understanding binary trees";
        messages[1] = "Binary trees are a way of structuring data in a computer system. ";
        messages[2] = "Each tree is made up of a number of nodes.";
        messages[3] = "Each node can have up to two child nodes known as children. It’s this property that makes the tree a BINARY tree.";
        messages[4] = "This applies to all of the nodes in a tree.";
        messages[5] = "At the beginning (top) of the tree, you’ll see a node known as the root node. This is the only node in the tree which isn’t a child of another node. ";
        messages[6] = "Nodes at the bottom of the tree, without any children are known as leaf nodes or sometimes the ‘edges’ of the tree.";
        messages[7] = "Now that you've got an idea of binary trees, touch this ball to take you through a traversal";
    }

    void OnDestroy()
    {
        TutorialLevel.iterate -= nextText;
    }
}
