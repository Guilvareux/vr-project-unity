﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;

public class TutorialLevel : MonoBehaviour
{
    public static TutorialLevel Instance {get; private set;}
    
    //Delegates and Events
    public delegate void traversal();
    public static event traversal hit;
    public delegate void nextText();
    public static event nextText iterate;

    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Boolean next = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Next");
    
    //3D Nodes
    public GameObject prefab;
    public List<GameObject> nodeList;
    private static List<Vector3> loc = new List<Vector3>();

    //Data Structures
    public GameObject text;
    
    private List<int> traversalOrder;
    //private int[] abusedNodes;
    int tutorialStage;
    
    private void Awake()
    {
        
        if(Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            //Destroy(gameObject);
        }
    }

    public void NextIteration(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        iterate();
    }



    // Start is called before the first frame update
    void Start()
    {
        next.AddOnStateDownListener(NextIteration, handType);
        Instantiate(text, new Vector3(-2.4f, 1.99f, 3.42f), new Quaternion(0, -0.2f, 0, 1));
        text.GetComponent<TextMeshPro>().SetText("It's working!");
        BTree tree = new BTree();
        int[] tutorialTree = {10, 30, 90, 39, 59, 23};
        tree.InsertArray(tutorialTree);
        traversalOrder = tree.getInOrderTraversal(tree.root);
        tutorialStage = 0;
        /*
        for(int i=0; i<traversalOrder.Count; i++)
        {
            Debug.Log(traversalOrder[i]);
        }*/
        drawTree(tree);
        iterate += tutorial;

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(iterate != null)
            {
                iterate();
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
        
    }

    public static void hitNode(int value)
    {
        Debug.Log(value);
    }

    public void drawTree(BTree tree)
    {
        Vector3 pos = new Vector3(0.7f, 3, 5);
        loc = tree.orderTree(tree.root, new Vector3(0.7f, 3, 5), 1.0f);
        drawNode(tree.root, pos);
    }

    public void drawNode(BNode node, Vector3 position)
    {
        Vector3 currentPos = loc[0];
        Debug.Log(loc.Count);
        GameObject go = (GameObject) Instantiate(prefab, loc[0], Quaternion.identity);
        go.GetComponent<CloneNode>().value = node.nodeValue;
        go.SetActive(false);
        nodeList.Add(go);
        loc.RemoveAt(0);
        if(node.Left != null)
        {
            drawNode(node.Left, currentPos);
        }
        if(node.Right != null)
        {
            drawNode(node.Right, currentPos);
        }                                 
    }

    public void tutorial()
    {
        tutorialStage++;
        if(tutorialStage == 2)
        {
            for(int i=0; i<traversalOrder.Count; i++)
            {
                nodeList[i].SetActive(true);
            }
        }
        else if(tutorialStage == 5)
        {
            nodeList[0].GetComponent<Renderer>().material.color = Color.red;
        }
        else if(tutorialStage == 6)
        {
            nodeList[0].GetComponent<Renderer>().material.color = Color.white;
            nodeList[2].GetComponent<Renderer>().material.color = Color.red;
            nodeList[4].GetComponent<Renderer>().material.color = Color.red;
            nodeList[5].GetComponent<Renderer>().material.color = Color.red;
        }
        else if(tutorialStage == 7)
        {
            SceneManager.LoadScene("ExampleTraversal", LoadSceneMode.Single);
            Destroy(gameObject);
        }
                
    }


}
