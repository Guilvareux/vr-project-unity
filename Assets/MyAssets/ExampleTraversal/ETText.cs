﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class ETText : MonoBehaviour
{
    int currentText = 0;
    public String[] messages = new String[100];
    // Start is called before the first frame update
    void Start()
    {
        setMessages();
        ExampleTraversalLevel.iterate += nextText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void nextText()
    {
            Debug.Log(messages[currentText]);
            currentText++;
            GetComponent<TextMeshPro>().SetText(messages[currentText]);
    }

    void setMessages()
    {
        messages[0] = "Take a look at the node in front of you, this is the root node";
        messages[1] = "All we know about this tree is that the root node contains the number 10";
        messages[2] = "Say we know that this tree holds a 59 somewhere, how would we find out where that was?";
        messages[3] = "Answer: Traversals";
        messages[4] = "Let's begin with a Pre-Order Traversal";
        messages[5] = "In each traversal, there are three operations: Move to the left child, Move to the right child and read the currently selected node";
        messages[6] = "In a Pre-Order Traversal, the order of these operations is Read, Move Left and Move Right";
        messages[7] = "Every time a node is visited, this operation starts on that node.";
        messages[8] = "Let's give it a go. So we start at the root node.";
        messages[9] = "We read the node value, It's a 10 so not the one we're looking for";
        messages[10] = "The next step is to move left. Since we've reached a new node we have to start the operation again. REMEMBER! the root node's operation isn't finished!";
        messages[11] = "We read this node, it's 30, and we move left again.";
        messages[12] = "This node is 39, still not right.";
        messages[13] = "Now that theres nowhere to move left to, we go up the tree to the last unfinished node";
        messages[14] = "Which is here.";
        messages[15] = "Now we have to finish this previous nodes operation. Move Right";
        messages[16] = "Read.";
        messages[17] = "Tadah! We've found it.";
        messages[18] = "Now, it's your turn! Start by touching the root node.";
        messages[19] = "";
        messages[20] = "Very good, now the next node.";
        messages[21] = "Fantastic!";
        messages[22] = "Keep going!";
        messages[23] = "Now try and finish off the rest. Remember Read, Left, Right.";
        messages[24] = "Yes!";
        messages[25] = "";
    }

    void OnDestroy()
    {
        ExampleTraversalLevel.iterate -= nextText;
    }
}
