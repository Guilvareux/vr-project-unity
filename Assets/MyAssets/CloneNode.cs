﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CloneNode : MonoBehaviour
{

    public delegate void traversal(int value, GameObject node);
    public static event traversal hit;
    public int value;
    private TextMeshPro textMesh;
    private bool hittable = false;
    public AudioClip positive;
    public AudioClip negative;
    // Start is called before the first frame update
    void Start()
    {
        textMesh = (TextMeshPro) gameObject.GetComponentInChildren<TextMeshPro>();
        textMesh.SetText(value.ToString());
        textMesh.GetComponent<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        while(counter < 5)
        {
            TutorialLevel.hitNode(value);
            counter = counter + 1;
        }*/
        
    }

    public void showValue(bool show)
    {
        MeshRenderer textShow = textMesh.GetComponent<MeshRenderer>();
        textShow.enabled = show;
    }

    void OnCollisionEnter(Collision collision)
    {
        Scene scene = SceneManager.GetActiveScene();
        if(hittable == true)
        {
            if(scene.name == "ExampleTraversal")
            {
                GameObject ETL = GameObject.Find("ETL");
                ETL.GetComponent<ExampleTraversalLevel>().firstTraversal(value, gameObject);            
            }
            else if(scene.name == "TimeTrial")
            {
                GameObject tt = GameObject.Find("TimeTrial");
                tt.GetComponent<TimeTrial>().traversal(value, gameObject);
            }
            else
            {
                GameObject tt = GameObject.Find("TLevel");
                tt.GetComponent<TraversalLevel>().firstTraversal(value, gameObject);
            }
            Debug.Log("HIT");
        }
    }

    public void setHittable(bool sethit)
    {
        hittable = sethit;
    }

    public void playPositive()
    {
        hittable = false;
        AudioSource audioplay = gameObject.GetComponent<AudioSource>();
        audioplay.clip = positive;
        audioplay.Play();
    }

    public void playNegative()
    {
        AudioSource audioplay = gameObject.GetComponent<AudioSource>();
        audioplay.clip = negative;
        audioplay.Play();
    }
}
