﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BNode
{
    public BNode Left = null;
    public BNode Right = null;
    public int nodeValue;
    public BNode(int value)
    {
        nodeValue = value; 
    }
}
