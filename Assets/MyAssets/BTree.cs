﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTree
{
    public int traversalLength = 6;
    public BNode root;
    public List<BNode> nodelist = new List<BNode>();
    private int treeHeight = 2;
    private List<int> tOrder = new List<int>();
    private List<Vector3> lOrder = new List<Vector3>();
    Dictionary<int, int> chances = new Dictionary<int, int>();

    private int nodesLeft;
    public BTree()
    {
        root = null;
        chances.Add(0, 0);
        chances.Add(1, 5);
        chances.Add(2, 20);
        chances.Add(3, 90);
    }

    public void InsertArray(int[] values)
    {
        BNode[] nodes = new BNode[values.Length];
        /*    
        for(int i=0; i<values.Length; i++)
        {
            //InsertSingleNode(values[i]);
            nodes[i] = new BNode(values[i]);
            nodelist.Add(nodes[i]);
        }*/
        root = new BNode(values[0]);
        root.Left = new BNode(values[1]);
        root.Right = new BNode(values[2]);
        root.Left.Left = new BNode(values[3]);
        root.Left.Right = new BNode(values[4]);
        root.Left.Right.Left = new BNode(values[5]);
        /*
        nodelist.Add(root);
        nodelist.Add(root.Left);
        nodelist.Add(root.Right);
        nodelist.Add(root.Left.Left);
        nodelist.Add(root.Left.Right);
        nodelist.Add(root.Left.Right.Left);*/
        //Debug.Log(root.Left.Right.Left);
    }

    public void createRandomTree()
    {
        List<int> randarr = new List<int>();
        for(int i=0; i<15; i++)
        {
            randarr.Add(Random.Range(1, 100));            
        }
        if(root != null)
        {
            root = null;
        }
        root = new BNode(randarr[0]);
        randarr.RemoveAt(0);
        fillRandomTree(root, randarr, 1);
        nodesLeft = 15;
        removeRandomNodes(root, 0);

    }
    public void fillRandomTree(BNode node, List<int> randarr, int height)
    {
        if(node.Left == null)
        {
            node.Left = new BNode(randarr[0]); 
            randarr.RemoveAt(0);
        }
        if(node.Right == null)
        {
            node.Right = new BNode(randarr[0]); 
            randarr.RemoveAt(0);
        }
        
        if(height < treeHeight)
        {
            fillRandomTree(node.Left, randarr, height + 1); 
            fillRandomTree(node.Right, randarr, height + 1);
        }
    }

    private void removeRandomNodes(BNode node, int height)
    {
        if(height <= treeHeight)
        {
            if(node.Left != null && node.Right != null)
            {
                if(Random.Range(0, 10) < 5)
                {
                    removeRandomNodes(node.Left, height + 1);
                    removeRandomNodes(node.Right, height + 1);
                }
                else
                {
                    removeRandomNodes(node.Right, height + 1);
                    removeRandomNodes(node.Left, height + 1);
                }

                if(Random.Range(0, 100) <= chances[height]){node.Left = null;}
                if(Random.Range(0, 100) <= chances[height]){node.Right = null;}
            }
        }
    }

    public List<int> getInOrderTraversal(BNode currentNode)
    {
        if(currentNode.Left != null)
        {
            getInOrderTraversal(currentNode.Left);                          
        }
        tOrder.Add(currentNode.nodeValue);
        if(currentNode.Right != null)
        {
            getInOrderTraversal(currentNode.Right);
        }
        return tOrder;
    }
    public List<int> getPreOrderTraversal(BNode currentNode)
    {
        tOrder.Add(currentNode.nodeValue);
        if(currentNode.Left != null)
        {
            getPreOrderTraversal(currentNode.Left);
        }
        if(currentNode.Right != null)
        {
            getPreOrderTraversal(currentNode.Right);
        }
        return tOrder;
    }
    public List<int> getPostOrderTraversal(BNode currentNode)
    {
        if(currentNode.Left != null)
        {
            getPostOrderTraversal(currentNode.Left);
        }
        if(currentNode.Right != null)
        {
            getPostOrderTraversal(currentNode.Right);
        }
        tOrder.Add(currentNode.nodeValue);
        return tOrder;
    }

    public List<Vector3> orderTree(BNode currentNode, Vector3 startLoc, float diff)
    {
        //List<Vector3> retList = new List<Vector3>();
        lOrder.Add(startLoc);
        if(currentNode.Left != null)
        {
            orderTree(currentNode.Left, startLoc + new Vector3(-diff/2, -0.25f, 0), diff/2);
        }
        if(currentNode.Right != null)
        {
            orderTree(currentNode.Right, startLoc + new Vector3(diff/2, -0.25f, 0), diff/2);
        }
        return lOrder;
    }

    /*

    private Vector3 nextRowLoc(Vector3 previous)
    {
        int x = previous.x;

        result = 

        return new Vector3(result, y, 0);
    }*/
}
