﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSTNode : MonoBehaviour
{
    public GameObject Left = null;
    public GameObject Right = null;
    public bool rightNode = false;
    public bool leftNode = false;
    public int row;
    public int column;
    public int value;
    private int currentMat;
    public bool nodeRead = false;
    
    // Start is called before the first frame update
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        SwitchMaterial();
    }

    void SwitchMaterial()
    {

    }
}
