﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSTTree : MonoBehaviour
{
    public GameObject root;
    public Vector3 rootLocation = new Vector3(0, 15, 0);
    public List<GameObject> nodeTree = new List<GameObject>();
    public int treeHeight;
    public List<int> traversalOrder = new List<int>();
    public GameObject prefab;
    private int traversalLength;

    private int distanceBetweenNodes = 30;


    void Start()
    {
        root = null;
        treeHeight = 3;
        debugger();
        //drawTree();
        //getInOrderTraversal((BSTNode) root.GetComponent<BSTNode>());
    }


    void debugger()
    {
        int[] debugarray = {10, 30, 90, 39, 59, 23};
        traversalLength = debugarray.Length;
        InsertArray(debugarray);
        //drawTree();
    }


    bool randomUnnamed(GameObject startObj, GameObject newObj, int row, int column)
    {
        BSTNode startNode = startObj.GetComponent<BSTNode>();


        if(row >= treeHeight)
        {
            //randomUnnamed(root, newNode, row, column);
            //return false;
            return true;
        }
        row++;
        if(Random.Range(0,10) < 5)
        {
            if(startNode.leftNode != false)
            {
                randomUnnamed(startNode.Left, newObj, row, column * 2);
            }
            else
            {
                newObj.GetComponent<BSTNode>().row = row;
                newObj.GetComponent<BSTNode>().column = column * 2;
                //nodeTree.Add(newObj);
                startNode.Left = newObj;
                Debug.Log("NODE LEFT!");
                return true;
            }
        }
        else
        {
            if(startNode.rightNode != false)
            {
                randomUnnamed(startNode.Right, newObj, row, column + 1);
            }
            else
            {
                newObj.GetComponent<BSTNode>().row = row;
                newObj.GetComponent<BSTNode>().column = column + 1;
                //nodeTree.Add(newObj);
                startNode.Right = newObj;
                Debug.Log("NODE RIGHT!");
                return true;
            }
        }
        return true;
    }

    void InsertSingleNode(int value)
    {
        int row = 0;
        GameObject newNode = (GameObject) Instantiate(prefab, rootLocation, Quaternion.identity);
        nodeTree.Add(newNode);
        BSTNode newNodeScript = (BSTNode) newNode.GetComponent<BSTNode>();
        newNodeScript.value = value;
        if(root == null)
        {
            root = newNode;
        }
        else
        {
            BSTNode current = (BSTNode) root.GetComponent<BSTNode>();
            int counter = 0;
            while(counter < traversalLength)
            {
                if(row > treeHeight)
                {
                    current = (BSTNode) root.GetComponent<BSTNode>();
                    row = 0;
                }
                if(Random.Range(0,10) < 5)
                {
                    if(current.leftNode != false)
                    {
                        current = (BSTNode) current.Left.GetComponent<BSTNode>();
                        row++;
                        continue;
                    }
                    else
                    {
                        current.Left = newNode;
                        current.leftNode = true;
                        counter++;
                    }
                }
                else
                {
                    if(current.rightNode != false)
                    {
                        current = (BSTNode) current.Right.GetComponent<BSTNode>();
                        row++;
                        continue;
                    }
                    else
                    {
                        current.Right = newNode;
                        current.rightNode = true;
                        counter++;
                    }
                }
            }      
        }
    }

    void InsertArray(int[] values)
    {
        /*
        for(int i=0; i<values.Length; i++)
        {
            generateSimpleNode()
        }
        */
        root = generateSimpleNode(values[0]);
        GameObject rRight = (GameObject) root.GetComponent<BSTNode>().Right;
        rRight = generateSimpleNode(values[2]);
        GameObject rLeft = root.GetComponent<BSTNode>().Left;
        GameObject rLeftLeft = (GameObject) rLeft.GetComponent<BSTNode>().Left;
        GameObject rLeftRight = (GameObject) rLeft.GetComponent<BSTNode>().Right;
        GameObject rLeftRightLeft = (GameObject) rLeftRight.GetComponent<BSTNode>().Left;
        rLeft = generateSimpleNode(values[1]);
        rLeftLeft = generateSimpleNode(values[3]);
        rLeftRight = generateSimpleNode(values[4]);
        rLeftRightLeft = generateSimpleNode(values[5]);

        nodeTree.Add(root);
        nodeTree.Add(rRight);
        nodeTree.Add(rLeft);
        nodeTree.Add(rLeftLeft);
        nodeTree.Add(rLeftRight);
        nodeTree.Add(rLeftRightLeft);
    }

    GameObject generateSimpleNode(int value)
    {
        GameObject newNode = (GameObject) Instantiate(prefab, rootLocation, Quaternion.identity);
        BSTNode script = (BSTNode) newNode.GetComponent<BSTNode>();
        script.value = value;
        return newNode;
    }



    // Update is called once per frame
    void Update()
    {
        
    }

    void getInOrderTraversal(BSTNode currentNode)
    {
        //BSTNode currentNode = (BSTNode) currentObject.GetComponent<BSTNode>();
        Debug.Log(currentNode.Left);
        Debug.Log(currentNode.Right);
        if(currentNode.leftNode != false)
        {
            getInOrderTraversal((BSTNode)currentNode.Left.GetComponent<BSTNode>());                          
        }
        traversalOrder.Add((int)currentNode.GetComponent<BSTNode>().value);
        Debug.Log("TRAVERSAL ADD");
        if(currentNode.rightNode != false)
        {
            getInOrderTraversal((BSTNode)currentNode.Right.GetComponent<BSTNode>());
        }
    }
    void getPreOrderTraversal(BSTNode currentNode)
    {
        traversalOrder.Add(currentNode.GetComponent<BSTNode>().value);
        if(currentNode.leftNode != false)
        {
            getPreOrderTraversal((BSTNode)currentNode.Left.GetComponent<BSTNode>());
        }
        if(currentNode.rightNode != false)
        {
            getPreOrderTraversal((BSTNode)currentNode.Right.GetComponent<BSTNode>());
        }
    }
    void getPostOrderTraversal(BSTNode currentNode)
    {
        if(currentNode.leftNode != false)
        {
            getPostOrderTraversal((BSTNode)currentNode.Left.GetComponent<BSTNode>());
        }
        if(currentNode.rightNode != false)
        {
            getPostOrderTraversal((BSTNode)currentNode.Right.GetComponent<BSTNode>());
        }
        traversalOrder.Add(currentNode.value);
    }

    /*
    void drawTree()
    {
        drawNode(root, rootLocation, Quaternion.identity);
    }

    void drawNode(GameObject node, Vector3 location, Quaternion rotation)
    {
        Instantiate(node, location, rotation);
        BSTNode nodeScript = (BSTNode) node.GetComponent<BSTNode>();
        if(nodeScript.leftNode != false)
        {
            drawNode(nodeScript.Left, location + new Vector3(20, -30, 0), rotation);
        }
        if(nodeScript.rightNode != false)
        {
            drawNode(nodeScript.Right, location + new Vector3(-20, -30, 0), rotation);
        }
        return;
    }*/

    Vector3 calculateNodeLocation(int row, int column)
    {
        if(row == 0 && column == 0)
        {
            return rootLocation;
        }
        int x;
        int y;               
        int nodenum = 2^row;
        int w = 50 * (1 - (row / 10));
        if(column >= nodenum)
        {
            x = column - (nodenum/2);
            x = x + (w * column) + w;           
        }
        else
        {
            x = column - (nodenum/2) - 1;
        }

        //200 = start location FIX NOW
        y = 200 - (row * distanceBetweenNodes);

        return new Vector3(x, y, 0);
    }

    void moveNodes()
    {
        for(int i=0; i<nodeTree.Count; i++)
        {
            nodeTree[i].GetComponent<BSTNode>();

        }
    }



    
}
